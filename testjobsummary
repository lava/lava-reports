#!/usr/bin/env python3

#  Copyright 2018 Linaro Limited
#            2023-2024 Collabora Limited
#  Author: Dave Pigott <dave.pigott@collabora.com>

#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# testjobsummary - output summary information about jobs run on a LAVA instance
from __future__ import annotations

import argparse
import datetime
import json
import netrc
import os
import re
import sys
from urllib.parse import urlencode

import requests


class RestFrameworkAuth(requests.auth.AuthBase):
    def __init__(self, token):
        self.token = token

    def __call__(self, r):
        r.headers["Authorization"] = f"Token {self.token}"
        return r


class ErrorMapping(object):
    def __init__(self):
        self.messages = {}

    def add_error_type(self, error_type):
        self.messages[error_type] = []

    def add_error_mapping(self, error_type, template, message):
        self.messages[error_type].append(
            {
                "template": re.compile(template),
                "message": message
            }
        )

    def get_mapping(self, error_type, message):
        entry = self.messages[error_type]
        result = ""

        if message is None:
            message = 'None'

        for i in entry:
            if re.match(i["template"], message):
                result = i["message"]

        if result == "":
            result = "No match for error type '%s', message '%s'" % (error_type, message)
            print(result, file=sys.stderr)

        return result


class LavaJobReport:
    def __init__(self, lava_hostname, mapfile, show_progress):
        self.lava_hostname = lava_hostname
        netrc_auth = netrc.netrc()
        lava_username, _, lava_token = netrc_auth.authenticators(self.lava_hostname)
        self.http_auth = RestFrameworkAuth(lava_token)

        self.show_progress = show_progress
        self.error_type_job = "Job"
        self.error_type_test = "Test"
        self.error_type_lava = "Bug"
        self.error_type_infrastructure = "Infrastructure"
        self.error_type_canceled = "Canceled"
        self.error_type_configuration = "Configuration"

        self.total_jobs_key = "total_jobs"
        self.total_errors_key = "total_errors"

        self.job_errors_key = "job_errors"
        self.test_errors_key = "test_errors"
        self.lava_errors_key = "lava_errors"
        self.infrastructure_errors_key = "infrastructure_errors"
        self.canceled_jobs_key = "canceled_jobs"
        self.configuration_errors_key = "configuration_errors"
        self.job_summary = {
            "summary": {
                self.total_jobs_key: 0,
                self.total_errors_key: 0,
                self.job_errors_key: 0,
                self.test_errors_key: 0,
                self.lava_errors_key: 0,
                self.infrastructure_errors_key: 0,
                self.canceled_jobs_key: 0,
                self.configuration_errors_key: 0
            },
            "devices": []
        }
        self.mapping = ErrorMapping()
        self.populate_errors(mapfile)
        self.log = []

    def populate_errors(self, mapfile):
        with open(mapfile) as data_file:
            data = json.load(data_file)

        for entry in data:
            current_type = entry["category"]
            self.mapping.add_error_type(current_type)
            for mapping in entry["maps"]:
                self.mapping.add_error_mapping(
                    current_type,
                    mapping['template'],
                    mapping['message'],
                )

    def gather_report(self, time_period_minutes, filter_list):

        initial_params = {
            "ordering": "-submit_time",
            "page_size": "1000",
            "submit_time_after": (
                datetime.datetime.utcnow()
                - datetime.timedelta(minutes=time_period_minutes)
            ).isoformat(),
        }
        next_url = (
            f"https://{self.lava_hostname}/api/v0.3-experimental/jobs/?"
            + urlencode(initial_params)
        )
        total_jobs_count = 0

        while next_url:
            data = requests.get(next_url, auth=self.http_auth).json()
            next_url = data["next"]
            results = data["results"]

            if not results:
                break

            for i, job_data in enumerate(results):

                if job_data["state"] != "Finished":
                    if self.show_progress:
                        print("Skipping not finished job:", job_data["id"])
                    continue

                if filter_list:
                    for j in filter_list:
                        if j in job_data['description'].lower():
                            self.add_job_details(job_data)
                            break
                else:
                    if 'lava-health' not in job_data['submitter']:
                        self.add_job_details(job_data)

            total_jobs_count += i+1
            if self.show_progress:
                print(f"Parsed {i+1} jobs", file=sys.stderr)
                print(f"Total jobs parsed: {total_jobs_count}", file=sys.stderr)

        return self.job_summary

    def add_job_details(self, job_info):
        self.job_summary["summary"]["total_jobs"] += 1

        device_entry = self.get_device_entry(
            job_info["requested_device_type_id"]
        )

        error_type = job_info["error_type"]
        if error_type != '' and error_type is not None:
            summary = self.job_summary["summary"]
            if error_type == self.error_type_job:
                summary[self.job_errors_key] += 1
            elif error_type == self.error_type_infrastructure:
                summary[self.infrastructure_errors_key] += 1
            elif error_type == self.error_type_test:
                summary[self.test_errors_key] += 1
            elif error_type == self.error_type_lava:
                summary[self.lava_errors_key] += 1
            elif error_type == self.error_type_canceled:
                summary[self.canceled_jobs_key] += 1
            elif error_type == self.error_type_configuration:
                summary[self.configuration_errors_key] += 1
            else:
                print("Unknown error type '%s'" % error_type, file=sys.stderr)
            self.job_summary["summary"]["total_errors"] += 1
            device_entry["total_errors"] += 1
            message_entry = self.get_message_entry(device_entry, job_info["error_type"], job_info["error_msg"])
            message_entry["count"] += 1
            try:
                message_entry["job_ids"][job_info["actual_device_id"]].append(job_info["id"])
            except KeyError:
                message_entry["job_ids"][job_info["actual_device_id"]] = [job_info["id"]]
        device_entry["total_jobs"] += 1

    def get_device_entry(self, device_type):
        result = {}

        device_list = self.job_summary["devices"]

        for entry in device_list:  # type: dict
            if entry["device_type"] == device_type:
                result = entry
                break

        if result == {}:
            result = {
                "device_type": device_type,
                "total_jobs": 0,
                "total_errors": 0,
                "categories": []
            }
            device_list.append(result)

        return result

    def get_message_entry(self, device_entry, error_type, error_message):
        result = {}
        mapped_message = self.mapping.get_mapping(error_type, error_message)

        for entry in device_entry["categories"]:
            if entry["error_type"] == error_type:
                entry["total_count"] += 1
                for i in entry["breakdown"]:
                    if i["error_message"] == mapped_message:
                        result = i
                        break
                if result == {}:
                    result = {"error_message": mapped_message, "count": 0, "job_ids": {}}
                    entry["breakdown"].append(result)
                    break

        if result == {}:
            result = {"error_message": mapped_message, "count": 0, "job_ids": {}}
            device_entry["categories"].append({"error_type": error_type,
                                               "total_count": 1,
                                               "breakdown": [
                                                   result
                                               ]})

        return result

    def print_summary(self, summary, device_details, summaryfile):
        total_jobs = summary[self.total_jobs_key]

        if not total_jobs:
            print("No qualifying jobs")
            return

        print("Total jobs:\t%s" % total_jobs, file=summaryfile)
        print("\tTotal errors:\t%s\t(%.2f%%)" % (summary[self.total_errors_key],
                                                 100 * summary[self.total_errors_key] / total_jobs),
              file=summaryfile)
        print("\tLAVA errors:\t%s\t(%.2f%%)" % (summary[self.lava_errors_key],
                                                100 * summary[self.lava_errors_key] / total_jobs),
              file=summaryfile)
        print("\tTest errors:\t%s\t(%.2f%%)" % (summary[self.test_errors_key],
                                                100 * summary[self.test_errors_key] / total_jobs),
              file=summaryfile)
        print("\tJob errors:\t%s\t(%.2f%%)" % (summary[self.job_errors_key],
                                               100 * summary[self.job_errors_key] / total_jobs),
              file=summaryfile)
        print("\tInfra errors:\t%s\t(%.2f%%)" % (summary[self.infrastructure_errors_key],
                                                 100 * summary[self.infrastructure_errors_key] / total_jobs),
              file=summaryfile)
        print("\tCancelled jobs:\t%s\t(%.2f%%)" % (summary[self.canceled_jobs_key],
                                                  100 * summary[self.canceled_jobs_key] / total_jobs),
              file=summaryfile)
        print("\tConfiguration errors:\t%s\t(%.2f%%)" % (summary[self.configuration_errors_key],
                                                  100 * summary[self.configuration_errors_key] / total_jobs),
              file=summaryfile)

        for entry in device_details:
            print(file=summaryfile)
            total_jobs = entry[self.total_jobs_key]
            print("Device type:\t%s" % entry["device_type"], file=summaryfile)
            print("Total jobs:\t%s" % total_jobs, file=summaryfile)
            print("Total errors:\t%s\t(%.2f%%)" % (entry[self.total_errors_key],
                                                   100 * entry[self.total_errors_key] / total_jobs),
                  file=summaryfile)
            for error_type in entry["categories"]:
                print("\tError type:\t%s" % error_type["error_type"], file=summaryfile)
                print("\tError count:\t%s\t(%.2f%%)" % (error_type["total_count"],
                                                        100 * error_type["total_count"] / total_jobs),
                      file=summaryfile)
                for detail in error_type["breakdown"]:
                    print("\t\tError:\t%s" % detail["error_message"], file=summaryfile)
                    print("\t\t\tCount:\t%s\t(%.2f%%)" % (detail["count"],
                                                          100 * detail["count"] / total_jobs),
                          file=summaryfile)
                    print("\t\t\tIDs:\t", file=summaryfile)

                    for device_entry in sorted(detail["job_ids"]):
                        print("\t\t\t%s:" % device_entry, file=summaryfile)
                        print("\t\t\t\t", end='', file=summaryfile)
                        id_count = 0
                        for job_id in sorted(detail["job_ids"][device_entry]):
                            print("%s " % job_id, end='', file=summaryfile)
                            id_count += 1
                            if id_count == 5:
                                print("\n\t\t\t\t", end='', file=summaryfile)
                                id_count = 0
                        print(file=summaryfile)


def main():
    parser = argparse.ArgumentParser(description='Get job failure report', add_help=False)
    parser.add_argument('--help', action='help', help='Show this help message and exit')
    parser.add_argument("-d", "--directory", type=str, default=".",
                        help="Top directory for report hierarchy (default is current dir)")
    parser.add_argument("-h", "--host", type=str, required=True,
                        help="LAVA instance path, e.g. validation.linaro.org")
    parser.add_argument("-j", "--jobfilter", type=str, default=None,
                        help="Command separated list of job descriptions to filter on")
    parser.add_argument("-m", "--mapfile", type=str, required=True,
                        help="Location of the message mapping file")
    parser.add_argument("-o", "--output_progress", action="store_true",
                        help="Output progress information to stderr")
    parser.add_argument("-p", "--period", type=int, default=10,
                        help="Time period to report on in minutes (default 10)")
    parser.add_argument("-r", "--report_word", type=str, required=True,
                        help="Report word for email - e.g. Daily, Weekly")
    options = parser.parse_args()

    reporter = LavaJobReport(options.host, options.mapfile, options.output_progress)

    report_time = datetime.datetime.utcnow()

    tag = options.host.split(".")[0]

    report_dir = "%s/%04d/%02d/%02d" % (options.directory, report_time.year, report_time.month, report_time.day)

    os.makedirs(report_dir, mode=0o755, exist_ok=True)

    report_file = "%s/%s-report-%s" % (report_dir, tag, options.period)

    if options.jobfilter is None:
        filter_list = []
    else:
        filter_list = options.jobfilter.split(",")

    report = reporter.gather_report(options.period, filter_list)

    reporter.print_summary(report["summary"], report["devices"], open(report_file, "w+"))

    print(json.dumps(report, sort_keys=True, indent=4), file=open("%s.json" % report_file, "w+"))

    if options.output_progress:
        print("\nReport in:\t%s\n" % report_file)


if __name__ == '__main__':
    main()
