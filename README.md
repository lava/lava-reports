# Lava Reports

Tools to generate reports of various sorts on a LAVA instance and it's front end CIs

## Available tools

Currently:

job_start_stats - generate a report on a per device-type basis, with analysis of the average times taken from submission to starting execution. It also provides information on the longest wait time, standard distribution, submitter and job priority.
